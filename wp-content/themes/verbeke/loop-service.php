<div class="loop-item--content">
	<div class="post-thumb">
		<div class="inner">
			<?php the_post_thumbnail('project'); ?>
		</div>
	</div>
	<div class="post-content">
	<!-- h3 deleten -->
		<?php the_content(); ?>
	<?php $link = get_field('services_link');
	if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
		?>
		<a class="link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
	<?php endif; ?>
	</div>
</div>
