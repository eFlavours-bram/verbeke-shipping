<?php 
/**
 * 	Template Name: Contact template
 *
*/
get_header(); ?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<div class="page-contact wrap">
	<div class="contact-header content-header">
		<h1 class="contact-header--title content-header--title"><?php the_title(); ?></h1>
		<div class="contact-header--content content-header--content"><?php remove_all_filters('the_content'); the_content(); ?></div>
	</div>
	<div class="content-section contact-section contact-section--team bg-blue">
		<?php
		$args = array('post_type' => 'team', 'posts_per_page' => -1);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) :
		?>
		<div class="loop loop--team">
		<?php
		while ( $query->have_posts() ) {
			$query->the_post();
			get_template_part('loop','team');
		}			
		?>
		</div>
		<?php 
		endif;
		wp_reset_postdata();
		?>
	</div>
	<div class="content-section contact-section contact-section--form bg-blue">
		<div class="contact-form">
			<div class="contact-form--left">
				<?php $logo = get_field('option_logo','option'); ?>
				<?php echo wp_get_attachment_image($logo); ?>
				<div class="contact-address">
					<?php if (get_field('option_bedrijfsnaam', 'option')): echo get_field('option_bedrijfsnaam', 'option') . '<br>'; endif; ?>
					<?php if (get_field('option_address', 'option')): echo get_field('option_address', 'option') . '<br>'; endif; ?>
					<?php if (get_field('option_city', 'option')): echo get_field('option_city', 'option'). '<br>'; endif; ?>
					<?php if (get_field('option_land', 'option')): echo get_field('option_land', 'option'); endif; ?>
				</div>
				<div class="contact-communication">
					<?php if (get_field('option_email', 'option')): echo '<a href="mailto:'.get_field('option_email', 'option').'">' . get_field('option_email', 'option') . '</a><br>'; endif; ?>
					<?php if (get_field('option_tel', 'option')): echo '<a href="tel:'.get_field('option_tel', 'option').'">' . get_field('option_tel', 'option') . '</a>'; endif; ?>
				</div>				
			</div>
			<div class="contact-form--right">
				<?php echo do_shortcode('[ninja_form id='.get_post_meta(get_the_id(),'ninja_forms_form',1).']'); ?>
			</div>			
		</div>
	</div>	
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>