<?php 
/**
 * 	Template Name: About template
 *
*/
get_header(); ?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<div class="page-about wrap">
	<div class="about-header content-header">
		<h1 class="about-header--title content-header--title"><?php the_title(); ?></h1>
		<div class="about-header--content content-header--content"><?php remove_all_filters('the_content'); the_content(); ?></div>
	</div>
	<?php
		$args = array('post_type' => 'history', 'posts_per_page' => -1);
		$query = new WP_Query( $args );
	?>	
	<div class="content-section about-section about-section--history bg-blue">
		<div class="wrap">
            <?php if ( $query->have_posts() ) : ?>
                <div class="timeline">
                    <div class="centerline"></div>
                    <div class="loop loop--history">
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <?php get_template_part('loop','history'); ?>
                        <?php endwhile; ?>
                    </div>
                </div>

            <?php endif; wp_reset_postdata(); ?>
        </div>
	</div>
	<?php
		$args = array('post_type' => 'project', 'posts_per_page' => 3);
		$query = new WP_Query( $args );
	?>
	<div class="content-section about-section projects-section projects-section--loop bg-blue">
        <div class="wrap">
        <?php if ( $query->have_posts() ) : ?>
            <div class="loop loop--project">
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <?php get_template_part('loop','project'); ?>
            <?php endwhile; ?>
            </div>
            <div class="history_bottom">
            <a class="link all-projects" href="<?php echo get_post_type_archive_link('project'); ?>"><?php _e('Bekijk alle projecten','verbeke'); ?></a>
            </div>
        <?php endif; wp_reset_postdata(); ?>
        </div>
    </div>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>