<div class="fleet-content wrap_slider">
	<div class="fleet-info">
		<h3><?php the_title(); ?></h3>
		<p class="excerpt"><?php the_excerpt(); ?></p>
		<?php the_content(); ?>
	</div>
	<div class="fleet-gallery slider fleet-slider">
		<?php
		$images = get_field('fleet_gallery');
		$size = 'large';
		if ($images) : ?>
			<?php foreach ($images as $image_id) : ?>
				<div class="slider-item">
					<?php echo wp_get_attachment_image($image_id, $size); ?>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
<div class="fleet-details">
	<?php $attributes = get_field('fleet_attributes'); ?>
	<?php if ($attributes) : $i = 0; ?>
		<div class="accordion">
			<?php foreach ($attributes as $attribute) : $i++ ?>
				<h3 data-image="image-<?php echo $i; ?>"><?php echo $attribute['title']; ?></h3>
				<div class="accordion-<?php echo $i; ?>">
					<?php if ($attribute['text']) : ?><p><?php echo $attribute['text']; ?></p><?php endif; ?>
					<?php $parameters = $attribute['row']; ?>
					<?php if ($parameters) : ?>
						<div class="parameters">
							<?php foreach ($parameters as $parameter) : ?>
								<div class="parameter-item">
									<span><?php echo $parameter['title']; ?></span>
									<span><?php echo $parameter['text']; ?></span>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach;
			$i = 0; ?>
		</div>
		<div class="image-container">
			<?php foreach ($attributes as $attribute) : $i++ ?>
				<?php if ($attribute['image']) : ?>
					<div class="image-<?php echo $i; ?>">
						<?php echo wp_get_attachment_image($attribute['image'], 'accordion'); ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>