<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?php bloginfo('name'); ?> | 
		<?php is_front_page() ? bloginfo('description') : wp_title(''); ?>
	</title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
	<div class="page-header wrap">
		<div class="page-header--left">
			<a href="<?php echo esc_url( home_url( '/home' ) ); ?>" class="logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">	
				<?php $logo = get_field('option_logo','option'); ?>
				<?php echo wp_get_attachment_image($logo); ?>
			</a>
		</div>
		<div class="page-header--right">
			<div class="desktop-nav">
				<div class="language-switcher">
					<?php echo language_selector(); ?>
				</div>
				<nav class="primary-menu" id="primary-menu-top">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false ) );  ?>
				</nav>
			</div>
			<div class="hamburger">
				<div class="line1 "></div>
				<div class="line2 "></div>
				<div class="line3 "></div>
                <!-- <svg class="inline-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="320px" height="220.5px" viewBox="0 0 32 22.5" enable-background="new 0 0 32 22.5" xml:space="preserve">
                    <g class="svg-menu-toggle">
                        <path class="bar" d="M20.945,8.75c0,0.69-0.5,1.25-1.117,1.25H3.141c-0.617,0-1.118-0.56-1.118-1.25l0,0
                            c0-0.69,0.5-1.25,1.118-1.25h16.688C20.445,7.5,20.945,8.06,20.945,8.75L20.945,8.75z">
                        </path>
                        <path class="bar" d="M20.923,15c0,0.689-0.501,1.25-1.118,1.25H3.118C2.5,16.25,2,15.689,2,15l0,0c0-0.689,0.5-1.25,1.118-1.25 h16.687C20.422,13.75,20.923,14.311,20.923,15L20.923,15z">
                        </path>
                        <path class="bar" d="M20.969,21.25c0,0.689-0.5,1.25-1.117,1.25H3.164c-0.617,0-1.118-0.561-1.118-1.25l0,0
                            c0-0.689,0.5-1.25,1.118-1.25h16.688C20.469,20,20.969,20.561,20.969,21.25L20.969,21.25z">
                        </path>
                        <rect width="44" height="44" fill="none"></rect>
                    </g>
                </svg> -->
            </div>				
		</div>
		<div class="mobile-nav">
			<div class="mobile-nav--top">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false ) );  ?>
			</div>
			<div class="mobile-nav--bottom">
				<?php echo language_selector(); ?>
			</div>						
		</div>
	</div>	 
</header>
<section class="page-content">