<?php
define('VERBEKE_VERSION', 1.0);

/*-----------------------------------------------------------------------------------*/
/*  Set the maximum allowed width for any content in the theme
/*-----------------------------------------------------------------------------------*/
if (!isset($content_width)) $content_width = 900;

/*-----------------------------------------------------------------------------------*/
/* Add Rss feed support to Head section
/*-----------------------------------------------------------------------------------*/
add_theme_support('automatic-feed-links');

/*-----------------------------------------------------------------------------------*/
/* Add post thumbnail/featured image support
/*-----------------------------------------------------------------------------------*/
add_theme_support('post-thumbnails');
add_image_size('accordion', 840, 450, true);

/*-----------------------------------------------------------------------------------*/
/* Register main menu for Wordpress use
/*-----------------------------------------------------------------------------------*/
register_nav_menus(
    array(
        'primary'    =>    __('Primary Menu', 'verbeke'),
        'footer'    =>    __('Footer Menu', 'verbeke'),
    )
);



/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function verbeke_scripts()
{

    // get theme styles
    wp_enqueue_style('dashicons');
    wp_enqueue_style('style.css', get_stylesheet_directory_uri() . '/style.css',  VERBEKE_VERSION);

    // add theme scripts
    wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', array("jquery"), '1.8.1', true);
    wp_enqueue_script('slick-js', get_template_directory_uri() . '/assets/js/slick.min.js', array("jquery-ui"), '1.8.1', true);
    wp_enqueue_script('tabtoac-js', get_template_directory_uri() . '/assets/js/jquery.responsive-accordion-tabs.min.js', array("jquery-ui"), '1.8.1', true);

    wp_enqueue_script('verbeke-js', get_template_directory_uri() . '/assets/js/scripts.js', array("jquery-ui"), VERBEKE_VERSION, true);
}
add_action('wp_enqueue_scripts', 'verbeke_scripts');

function akbel_scripts_1()
{
    if (!is_admin()) {
        wp_enqueue_script('jquery-ui-accordion');
        wp_enqueue_script(
            'custom-accordion',
            get_stylesheet_directory_uri() . '/assets/js/accordion.js',
            array('jquery')
        );
    }
}
add_action('wp_enqueue_scripts', 'akbel_scripts_1');

// function akbel_scripts_2()
// {
//     if (!is_admin()) {
//         wp_enqueue_script('jquery-ui-tabs');
//         wp_enqueue_script(
//             'custom-tabs',
//             get_stylesheet_directory_uri() . '/assets/js/tabs.js',
//             array('jquery')
//         );
//     }
// }
// add_action('wp_enqueue_scripts', 'akbel_scripts_2');

function akbel_scripts_3()
{
    if (!is_admin()) {
        wp_enqueue_script('responsive-accordion-tabs');
        wp_enqueue_script(
            'custom-tabs',
            get_stylesheet_directory_uri() . '/assets/js/tabs.js',
            array('jquery')
        );
    }
}



/*-----------------------------------------------------------------------------------*/
/* Register Post Types
/*-----------------------------------------------------------------------------------*/

function verbeke_taxonomies()
{
    $labels = array(
        'name'               => __('Services', 'verbeke'),
        'singular_name'      => __('Service', 'verbeke'),
        'menu_name'          => __('Services', 'verbeke'),
        'name_admin_bar'     => __('Service', 'verbeke'),
        'add_new'            => __('Add New', 'verbeke'),
        'add_new_item'       => __('Add New Service', 'verbeke'),
        'new_item'           => __('New Service', 'verbeke'),
        'edit_item'          => __('Edit Service', 'verbeke'),
        'view_item'          => __('View Service', 'verbeke'),
        'all_items'          => __('All Services', 'verbeke'),
        'search_items'       => __('Search Services', 'verbeke'),
        'parent_item_colon'  => __('Parent Service', 'verbeke'),
        'not_found'          => __('No Services found.', 'verbeke'),
        'not_found_in_trash' => __('No Services found in trash.', 'verbeke')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'services', 'with_front' => false),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 21,
        'menu_icon'             => 'dashicons-admin-tools',
        'supports'           => array('title', 'editor', 'thumbnail','excerpt')
    );
    register_post_type('service', $args);

    $labels = array(
        'name'               => __('Projects', 'verbeke'),
        'singular_name'      => __('Project', 'verbeke'),
        'menu_name'          => __('Projects', 'verbeke'),
        'name_admin_bar'     => __('Project', 'verbeke'),
        'add_new'            => __('Add New', 'verbeke'),
        'add_new_item'       => __('Add New Project', 'verbeke'),
        'new_item'           => __('New Project', 'verbeke'),
        'edit_item'          => __('Edit Project', 'verbeke'),
        'view_item'          => __('View Project', 'verbeke'),
        'all_items'          => __('All Projects', 'verbeke'),
        'search_items'       => __('Search Projects', 'verbeke'),
        'parent_item_colon'  => __('Parent Project', 'verbeke'),
        'not_found'          => __('No Projects found.', 'verbeke'),
        'not_found_in_trash' => __('No Projects found in trash.', 'verbeke')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'projects', 'with_front' => false),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 22,
        'menu_icon'             => 'dashicons-portfolio',
        'supports'           => array('title', 'editor', 'thumbnail', 'excerpt')
    );
    register_post_type('project', $args);

    $labels = array(
        'name'               => __('Fleet', 'verbeke'),
        'singular_name'      => __('Fleet', 'verbeke'),
        'menu_name'          => __('Fleet', 'verbeke'),
        'name_admin_bar'     => __('Fleet', 'verbeke'),
        'add_new'            => __('Add New', 'verbeke'),
        'add_new_item'       => __('Add New Ship', 'verbeke'),
        'new_item'           => __('New Ship', 'verbeke'),
        'edit_item'          => __('Edit Ship', 'verbeke'),
        'view_item'          => __('View Ship', 'verbeke'),
        'all_items'          => __('All Ships', 'verbeke'),
        'search_items'       => __('Search Ships', 'verbeke'),
        'parent_item_colon'  => __('Parent Ship', 'verbeke'),
        'not_found'          => __('No Ships found.', 'verbeke'),
        'not_found_in_trash' => __('No Ships found in trash.', 'verbeke')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'fleet', 'with_front' => false),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 23,
        'menu_icon'             => 'dashicons-car',
        'supports'           => array('title', 'editor', 'thumbnail', 'excerpt')
    );
    register_post_type('fleet', $args);

    $labels = array(
        'name'               => __('Team', 'verbeke'),
        'singular_name'      => __('Team', 'verbeke'),
        'menu_name'          => __('Team', 'verbeke'),
        'name_admin_bar'     => __('Team', 'verbeke'),
        'add_new'            => __('Add New', 'verbeke'),
        'add_new_item'       => __('Add New Team member', 'verbeke'),
        'new_item'           => __('New Team member', 'verbeke'),
        'edit_item'          => __('Edit Team member', 'verbeke'),
        'view_item'          => __('View Team member', 'verbeke'),
        'all_items'          => __('All Team members', 'verbeke'),
        'search_items'       => __('Search Team members', 'verbeke'),
        'parent_item_colon'  => __('Parent Team member', 'verbeke'),
        'not_found'          => __('No Team members found.', 'verbeke'),
        'not_found_in_trash' => __('No Team members found in trash.', 'verbeke')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'team', 'with_front' => false),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 24,
        'menu_icon'             => 'dashicons-groups',
        'supports'           => array('title', 'thumbnail')
    );
    register_post_type('team', $args);

    $labels = array(
        'name'               => __('History', 'verbeke'),
        'singular_name'      => __('History', 'verbeke'),
        'menu_name'          => __('History', 'verbeke'),
        'name_admin_bar'     => __('History', 'verbeke'),
        'add_new'            => __('Add New', 'verbeke'),
        'add_new_item'       => __('Add New History item', 'verbeke'),
        'new_item'           => __('New History item', 'verbeke'),
        'edit_item'          => __('Edit History item', 'verbeke'),
        'view_item'          => __('View History item', 'verbeke'),
        'all_items'          => __('All History items', 'verbeke'),
        'search_items'       => __('Search History items', 'verbeke'),
        'parent_item_colon'  => __('Parent History item', 'verbeke'),
        'not_found'          => __('No History items found.', 'verbeke'),
        'not_found_in_trash' => __('No History items found in trash.', 'verbeke')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'history', 'with_front' => false),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 25,
        'menu_icon'             => 'dashicons-calendar-alt',
        'supports'           => array('title', 'editor', 'thumbnail')
    );
    register_post_type('history', $args);
}
add_action('init', 'verbeke_taxonomies', 0);

function verbeke_admin_menu()
{
    remove_menu_page('edit-comments.php');
}

add_action('admin_menu', 'verbeke_admin_menu');

/*-----------------------------------------------------------------------------------*/
/* Add actions/filters/hooks
/*-----------------------------------------------------------------------------------*/

function custom_excerpt_length( $length ) {
    return 25;
    }
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more', 21 );



/*-----------------------------------------------------------------------------------*/
/* ACF functions
/*-----------------------------------------------------------------------------------*/

if (function_exists('acf_add_options_page')) {

    acf_add_options_page();
}

/*-----------------------------------------------------------------------------------*/
/* Custom functions
/*-----------------------------------------------------------------------------------*/

function language_selector($full = "", $classes = "")
{
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    $output = "";
    if ($classes) $classes = ' ' . $classes;
    if (!empty($languages)) {
        $output = '<ul class="lang-list' . $classes . '">';
        foreach ($languages as $l) {
            $class = ($l['active']) ? ' class="active"' : '';
            $name = ($full ? $l['native_name'] : strtoupper($l['language_code']));
            $output .= '<li' . $class . '><a href="' . $l['url'] . '">' . $name . '</a>';
        }
        $output .= '</ul>';
    }
    return $output;
}

function load_more($posttype = "", $wp_query = array())
{
    if (!$wp_query) {
        global $wp_query;
    }
    if (!$posttype) {
        $posttype = $wp_query->query['post_type'];
    }
    $display = ($wp_query->max_num_pages > 1) ? 'inline-block' : 'none';
    echo '<div class="loadmore" style="display:' . $display . '"><a class="link-loadmore" data-type="' . $posttype . '" data-page="1" data-maxpage="' . $wp_query->max_num_pages . '">' . __('Load more projects', 'verbeke') . '</a></div>';
}
