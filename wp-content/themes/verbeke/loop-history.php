<article class="loop-item loop-item--history">
	<div class="post-content">
		<?php if (get_field('history_year')) : ?><span class="year"><?php the_field('history_year'); ?></span><?php endif; ?>
		<?php if (get_field('history_title')) : ?><h3><?php the_field('history_title'); ?></h3><?php endif; ?>
		<?php the_content(); ?>
	</div>
	<div class="post-thumb">
		<div class="inner">
			<?php the_post_thumbnail('history'); ?>
		</div>
	</div>
	<div class="post-dot"></div>	
</article>