<div class="team-member">
	<div class="team-member--photo"><?php the_post_thumbnail('team'); ?></div>
	<h3><?php the_title(); ?></h3>
	<span class="team-function"><?php echo get_field('team_function'); ?></span>
	<span class="team-mobile"><span><?php _e('GSM','verbeke'); ?></span><?php echo get_field('team_mobile'); ?></span>
	<span class="team-tel"><span><?php _e('TEL','verbeke'); ?></span><?php echo get_field('team_tel'); ?></span>
	<span class="team-fax"><span><?php _e('FAX','verbeke'); ?></span><?php echo get_field('team_fax'); ?></span>
	<a href="mailto:<?php echo get_field('team_email'); ?>"><?php echo get_field('team_email'); ?></a>
</div>