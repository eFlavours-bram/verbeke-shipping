<?php get_header(); ?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<div class="page">

<?php
if( have_rows('content_builder') ): while ( have_rows('content_builder') ) : the_row();
switch (get_row_layout()) {
	case 'full_width_image':
	$image = get_sub_field('fwi_image');
	$text = get_sub_field('fwi_text');
	?>
	<div class="content-section fullwidth-section fullwidth_image-section">
		<div class="background-image"><?php echo wp_get_attachment_image($image,'wide'); ?></div>
		<?php if ($text) : ?><h1 class="site-title"><?php echo $text; ?></h1><?php endif; ?>
	</div>
	<?php break;
	case 'Content_block':
	$fw = get_sub_field('cb_fw');
	$bg = get_sub_field('cb_background');
	$content = get_sub_field('cb_content');
	?>
	<div class="content-section content_block-section<?php if ($fw) echo ' fullwidth-section'; ?><?php if ($bg) echo ' bg-'.$bg; ?>">
		<?php if (!$fw) : ?>
		<div class="wrap">
			<div class="content_block">
				<div class="left bg-yellow">
					<?php if ( $content['title'] ) :?><h3><?php echo $content['title']; ?></h3><?php endif; ?>
					<?php if ( $content['text'] ) :?><p><?php echo $content['text']; ?></p><?php endif; ?>
					<?php 
					if( $content['link']): 
						$link_url = $content['link']['url'];
						$link_title = $content['link']['title'];
						$link_target = $content['link']['target'] ? $content['link']['target'] : '_self';
					?>
					<a class="link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					<?php endif; ?>						
				</div>
				<div class="right">
					<?php echo wp_get_attachment_image($content['image'],'contentblock'); ?>
				</div>
			</div>
		</div>
		<?php else : ?>
		<div class="content_block content_block-fullwidth">
			<div class="left">
				<?php echo wp_get_attachment_image($content['image'],'contentblockfw'); ?>	
			</div>
			<div class="right bg-darkblue font-white">
				<?php if ( $content['title'] ) :?><h3><?php echo $content['title']; ?></h3><?php endif; ?>
				<?php if ( $content['text'] ) :?><p><?php echo $content['text']; ?></p><?php endif; ?>
				<?php 
				if( $content['link']): 
					$link_url = $content['link']['url'];
					$link_title = $content['link']['title'];
					$link_target = $content['link']['target'] ? $content['link']['target'] : '_self';
				?>
				<a class="link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; ?>					
			</div>
		</div>
		<?php endif; ?>		
	</div>				
	<?php break;
	case 'custom_post_block':
	$title = get_sub_field('cpb_title');
	$text = get_sub_field('cpb_text');
	$link = get_sub_field('cpb_link');
	$showlink = get_sub_field('cpb_showlink');
	$cpt = get_sub_field('cpb_cpt');
	$bg = get_sub_field('cpb_background');
	?>
	<div class="content-section cpt_block-section<?php if ($bg) echo ' bg-'.$bg; ?>">
		<div class="wrap home_wrap">
			<h3><?php echo $title; ?></h3>
			<?php if ($text) : ?><p><?php echo $text; ?></p><?php endif; ?>
			<?php if ($link) {
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self'; }
			?>
			<?php if ($showlink && $link) : ?><a class="link link-top" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a><?php endif; ?>
			<?php
			$query = new WP_Query(array('post_type' => $cpt, 'posts_per_page' => 3));
			if ( $query->have_posts() ) : ?>
			<div class="loop loop--project">
            <?php while ($query->have_posts()) : $query->the_post(); ?>
                <?php get_template_part('loop','project'); ?>
			<?php endwhile; ?>
			<?php endif; wp_reset_postdata(); ?>
            </div>			
			<?php if (!$showlink && $link) : ?><a class="link link-bottom" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a><?php endif; ?>	
		</div>	
	</div>	
	<?php break;
}
endwhile; else : ?>
	<div class="content-header">
		<h1 class="content-header--title"><?php the_title(); ?></h1>
	</div>
	<div class="content-section">
		<div class="wrap">
			<?php the_content(); ?>
		</div>
	</div>
<?php endif; ?>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>	