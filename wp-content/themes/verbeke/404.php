<?php

/**
 * 404 pages (Not Found)
 *
 */

get_header(); ?>
<section class="t-404 wrap" id="404">

	<div class="t-404-wrapper">
		<div class="t-404-inner">
			<div class="left">
				<h1>What happens now?</h1>
				<p>Something went wrong. Either you clicked a link that does not exist which is our fault or some "internet stuff" happend.</p>
				<h2>Dont worry, we are here to help you</h2>
				<p><a href="<?php echo esc_url(home_url('/')); ?>" title="Take me back home!" rel="home" class="link-arrow">Take me to home</a>, I want to try again</p>
			</div>
			<div class="right">
			<img src="<?php echo home_url(); ?>/wp-content/uploads/2020/09/history80@2x.jpg">

			</div>

		</div>

	</div>

</section>
<?php get_footer(); ?>