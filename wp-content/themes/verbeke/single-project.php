<?php get_header(); ?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<div class="page-project wrap">

	<div class="project-header content-header">
		<h1 class="project-header--title content-header--title"><?php the_title(); ?></h1>
		<div class="project-header--content content-header--content"><?php the_content(); ?></div>
    </div>
    
	<div class="content-section project-section project-section--slider">
		<div class="project-slider">
		<?php 
		$images = get_field('project_gallery');
		$size = 'full';
		if( $images ): ?>
			<?php foreach( $images as $image_id ): ?>
			<div class="slider-item">
				<?php echo wp_get_attachment_image( $image_id, $size ); ?>
			</div>
			<?php endforeach; ?>
		<?php else: ?>	
			<div class="slider-item"><?php the_post_thumbnail($size); ?></div>
		<?php endif; ?>		
		</div>
	</div>

	<div class="content-section project-section project-section--footer bg-blue ">
		<div class="wrap">
			<div class="project-cta">
				<span class="project-cta--title"><?php echo get_field('option_cta_title','option');?></span>
				<?php 
				$link = get_field('option_cta_link','option');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="btn btn--invert" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; ?>
			</div>
			<div class="project-nav">
				<a href="<?php echo get_post_type_archive_link('project'); ?>" title="<?php _e('All projects','verbeke'); ?>"><?php _e('All projects','verbeke'); ?></a>
				<?php $next_post = get_previous_post();
					if($next_post) {
						$title = strip_tags(str_replace('"', '', $next_post->post_title));
						$url = get_permalink($next_post->ID);
					} else {
						$first = get_posts(array('numberposts' => 1, 'post_type' => 'project'));
						$url = get_permalink($first[0]->ID); 
						$title = get_the_title($first[0]->ID);
					} ?>
				<a href="<?php echo $url; ?>" title="<?php echo $title; ?>" rel="next"><?php _e('Next project','verbeke');?></a>
			</div>
		</div>
    </div>	
    
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>