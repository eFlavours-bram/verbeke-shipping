</section>
<footer>
    <div class="page-footer">
        <div class="page-footer-wrapper wrap">
            <div class="page-footer-left">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo logo-wit" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">	
                    <?php $logo = get_field('option_logo_wit','option'); ?>
                    <?php echo wp_get_attachment_image($logo); ?>
                </a>
                <div class="footer-contact">
                    <div class="footer-contact-left">
                        <?php the_field('option_bedrijfsnaam' , 'option'); ?><br>
                        <?php the_field('option_address', 'option'); ?><br>
                        <?php the_field('option_city', 'option'); ?>
                    </div>
                    <div class="footer-contact-right">
                        <a href="mailto:<?php the_field('option_email' , 'option'); ?>"><?php the_field('option_email' , 'option'); ?></a><br>
                        <?php the_field('option_tel', 'option'); ?><br>
                        <?php the_field('option_vat', 'option'); ?>
                    </div>
                </div>              
            </div>
            <div class="page-footer-right">
                <nav class="primary-menu">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false ) );  ?>
                </nav>
                <span class="footer-title"><?php the_field('option_footer_title' , 'option'); ?></span>
                <div class="footer-content">
                    <?php the_field('option_footer_text' , 'option'); ?>
                </div>
            </div> 
        </div>
    </div>
    <div class="page-credentials">
        <div class="page-credentials-wrapper wrap">
            <div class="page-credentials-left">
                <nav class="footer-menu">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer', 'container' => false ) );  ?>
                </nav>	
                <div class="language-switcher">
                    <?php echo language_selector('full'); ?>
                </div>                              
            </div>
            <div class="page-credentials-right">
                <?php _e('Webside door','verbeke'); ?><a href="https://www.wemake.be" target="_blank">We make.</a>
            </div>             
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>