<?php 
/**
 * 	Template Name: Service template
 *
*/
get_header(); ?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<div class="page-service wrap">
	<div class="service-header content-header">
		<h1 class="service-header--title content-header--title"><?php the_title(); ?></h1>
		<div class="service-header--content content-header--content"><?php remove_all_filters('the_content'); the_content(); ?></div>
	</div>
	<div class="content-section service-section service-section--services bg-blue">
		<?php
		$args = array('post_type' => 'service', 'posts_per_page' => -1);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) : $i=0; $count = $query->found_posts; 
		?>
		<ul class="slider-nav">
		<?php
		while ( $query->have_posts() ) { $i++;
			$query->the_post(); ?>
			<?php if ($i<=1) : ?>
				<li><a class="active"><?php the_title(); ?></a></li>
			<?php else : ?>
				<li><a><?php the_title(); ?></a></li>
			<?php endif; ?>
		<?php }	?>
		</ul>	
		<div class="loop loop--service service-slider slider wrap">
		<?php
		$i=0;
		while ( $query->have_posts() ) { $i++;
			$query->the_post();?>
			<article class="loop-item slider-item loop-item--service">
				<?php get_template_part('loop','service'); ?>
				<div class="loop-item--index"><?php echo $i; ?>/<?php echo $count; ?></div>
			</article>
		<?php }			
		?>
		</div>
		<?php 
		endif;
		wp_reset_postdata();
		?>
	</div>
	<div class="content-section project-section project-section--footer service-section service-section--footer bg-blue ">
		<div class="wrap">
			<div class="project-cta service-cta">
				<span class="project-cta--title service-cta--title"><?php echo get_field('option_cta_title','option');?></span>
				<?php 
				$link = get_field('option_cta_link','option');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="btn btn--invert" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; ?>
			</div>
		</div>
	</div>	
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>