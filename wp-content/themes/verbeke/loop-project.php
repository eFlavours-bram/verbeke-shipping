<?php $pid = get_the_id(); ?>
<?php $cpt = get_post_type(); switch ($cpt) {
	case 'project':
		$label = __('Bekijk het project', 'verbeke');
		$link = get_permalink();
		break;
	case 'service':
	$pages = get_pages(array(
		'meta_key' => '_wp_page_template',
		'meta_value' => 'page-service.php'
	));
	foreach($pages as $page){
		$pageid = $page->ID;
	}					
	$label = __('Bekijk de dienst', 'verbeke');
	$link = get_permalink($pageid).'#'.get_post_field( 'post_name',$pid);
	break;
} ?>
<article class="loop-item loop-item--project">
	<a href="<?php echo $link; ?>" title="<?php echo get_the_title(); ?>">
		<div class="post-thumb">
			<div class="inner">
				<?php the_post_thumbnail('project'); ?>
			</div>
		</div>
		<h4><?php the_title(); ?></h4>
		<p class="img_paragraf"><?php the_excerpt(); ?></p>
		<span class="link"><?php echo $label; ?></span>
	</a>
</article>