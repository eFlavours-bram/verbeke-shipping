// met javascript:
// const mobileNavSlide = () =>{
// 	const hamburger = document.querySelector(".hamburger")
// 	const nav = document.querySelector(".mobile-nav")

// 	hamburger.addEventListener("click", ()=> {
// 		nav.classList.toggle("mobile-nav-active")
// 		hamburger.classList.toggle("toggle")
// 	})
// }
// mobileNavSlide()

(function ($, root, undefined) {
  $(document).ready(function () {
    $(".hamburger").click(function () {
      $(".mobile-nav").toggleClass("expand");
      $(".hamburger").toggleClass("toggle");
    });
    $(".project-slider").slick({
      dots: true,
    });
    $(".service-slider").slick({
      dots: false,
      fade: false,
      speed: 500,
    });
    $(".fleet-slider").slick({
      dots: false,
      speed: 500,
    });
  });

  $(".slider-nav a").click(function (e) {
    e.preventDefault();
    slideIndex = $(this).parent().index();
    $(".slider-nav a").removeClass("active");
    $(this).addClass("active");
    $(".slider").slick("slickGoTo", parseInt(slideIndex));
  });

  $(".image-container > div")
    .not(":first")
    .each(function () {
      $(this).hide();
    });

  $(".fleet-details h3").click(function () {
    var image = $(this).attr("data-image");
    $(".image-container > div").each(function () {
      $(this).fadeOut(1000);
    });
    $(this)
      .closest(".fleet-details")
      .find("." + image)
      .fadeIn(1000);
  });

  var $accordionTabs = $(".accordion-tabs");
  $accordionTabs.accordionTabs(
    {
      mediaQuery: "(min-width: 768px)",
    },
    {
      header: "h4",
      heightStyle: "content",
      collapsible: true,
    },
    {
      show: {
        effect: "fade",
      },
    }
  );
})(jQuery, this);

//  $( window ).resize(function() {
// 	$( ".project-slider" ).slick();
// 	});

// $(".ui-tabs-anchor").click (function(){
//   $(this).toggleClass (".is-selected");
// });
