jQuery(document).ready(function($){
    var icons = {
        header: "iconClosed",
        activeHeader: "iconOpen"
    }
    $(".accordion").accordion({
        collapsible:true,
        //active:false,
        heightStyle:"content",
        icons:icons,
    });

});