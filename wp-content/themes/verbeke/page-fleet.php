<?php

/**
 * 	Template Name: Fleet template
 *
 */
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="page-fleet grote_wrap">
			<div class="wrap">
			<div class="fleet-header content-header">
				<h1 class="fleet-header--title content-header--title"><?php the_title(); ?></h1>
				<div class="fleet-header--content content-header--content"><?php remove_all_filters('the_content');
																			the_content(); ?></div>
			</div>
			</div>
			<?php
			$args = array('post_type' => 'fleet', 'posts_per_page' => -1);
			$query = new WP_Query($args);
			?>
			<?php if ($query->have_posts()) :  $i = 0; ?>
				<div class="content-section fleet-section fleet-section--tabs bg-blue">			
						<div class="fleet-tabs accordion-tabs">
							<ul class="wrap accordion-tab-headings">
								<?php while ($query->have_posts()) : $query->the_post();
									$i++ ?>
									<li>
										<a href="#<?php echo 'tab-' . $i; ?>" title="<?php echo get_the_title(); ?>">
											<div class="post-thumb-fleet">
												<div class="inner-fleet">
													<?php the_post_thumbnail('tab'); ?>
												</div>
											</div>
											<h4><?php the_title(); ?></h4>
										</a>
									</li>
								<?php endwhile;
								$i = 0; ?>
							</ul>
							<div class="accordion-tab-content">
							<?php while ($query->have_posts()) : $query->the_post();
								$i++; ?>
								<div id="<?php echo 'tab-' . $i; ?>" class="loop-item loop-item--fleet">
									<h4 class="accordion-title"><?php the_title(); ?></h4>
									<div class="wrap">
										<?php get_template_part('loop', 'fleet'); ?>
									</div>
								</div>
							<?php endwhile; ?>
							</div>
						</div>
				
				</div>
				<!--<div class="content-section fleet-section fleet-section--loop">
        <div class="wrap">
			<div class="loop loop--fleet">
				<?php while ($query->have_posts()) : $query->the_post();
					$i++; ?>
				<article id="<?php echo 'tab-' . $i; ?>" class="loop-item loop-item--fleet">
					<?php get_template_part('loop', 'fleet'); ?>
				</article>
				<?php endwhile; ?>
            </div>
        </div>
    </div>-->
			<?php endif;
			wp_reset_postdata();  ?>
			<div class="content-section fleet-section fleet-section--footer">
				<div class="wrap">
					<div class="project-cta fleet-cta">
						<span class="project-cta--title fleet-cta--title"><?php echo get_field('option_cta_title', 'option'); ?></span>
						<?php
						$link = get_field('option_cta_link', 'option');
						if ($link) :
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
							<a class="btn btn--invert" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
<?php endwhile;
endif; ?>
<?php get_footer(); ?>