<?php get_header(); ?>
<div class="page-projects wrap">

	<div class="projects-header content-header">
		<h1 class="projects-header--title content-header--title"><?php the_field('option_projectarchive_title','option'); ?></h1>
		<div class="projects-header--content content-header--content"><?php the_field('option_projectarchive_text','option'); ?></div>
    </div>
    
	<div class="content-section projects-section projects-section--loop bg-blue">
        <div class="wrap">
        <?php if (have_posts()): ?>
            <div class="loop loop--project">
            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('loop','project'); ?>
            <?php endwhile; ?>
            </div>
            <?php load_more('project'); ?>
        <?php endif; ?>
        </div>
    </div>
    
</div>
<?php get_footer(); ?>